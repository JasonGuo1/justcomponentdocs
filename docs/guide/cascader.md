

## cascader 级联选择


<iframe width="100%" height="300px" src="http://39.101.72.88?type=cascader"></iframe>

:::demo 使用`disabled`定义 dropselect 是否禁用。

```html
    <div class="cascader-wrapper">
        <JustCacsader  />
        <JustCacsader  disabled />
        <JustDropSelect type="single-cascader" />
        <JustDropSelect type="single-cascader" disabled />
    </div>
```
:::

