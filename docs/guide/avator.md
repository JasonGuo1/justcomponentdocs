## avator 头像

用来代表用户或事物，支持图片、图标或字符展示。

### 基础用法

头像有三种尺寸，两种形状可选。

size 可以设置为数值。
<iframe width="100%" height="650px" src="http://39.101.72.88/?=avatar"></iframe>
:::demo 使用`small`、`default`、`large`属性来定义 avatar 的尺寸。size 可以设置为数值，形状包括`square`和默认的`circle`两种。


```html
<div class="c1">
          <avatar :src="imgUrl" size="small" alt="小头像" />
          <avatar :src="imgUrl" size="default" />
          <avatar :src="imgUrl" size="large" />
          <avatar :src="imgUrl" :size="100"  />
        </div>
<div class="c2">
          <avatar :src="imgUrl" size="small" shape="square" />
          <avatar :src="imgUrl" size="default" shape="square" />
          <avatar :src="imgUrl" size="large" shape="square" />
          <avatar :src="imgUrl" :size="100" shape="square" />
        </div>
```

:::

### 类型

已支持两种类型：图片、 以及字符，其中字符型可以自定义背景色。
待补充支持类型：ICON，可自定义图标颜色及背景色

:::demo 已支持两种类型：图片、 以及字符，其中字符型可以自定义背景色。
待补充支持类型：ICON，可自定义图标颜色及背景色

```html
<avatar size="default">Hi</avatar>
<avatar size="default" color="white" background-color="gray">Hi</avatar>
<avatar :src="imgUrl" size="default" shape="square" />
```

:::

### 不同尺寸

avator 头像提供除了数值以外的三种尺寸，可以在不同场景下选择合适的按钮尺寸。

:::demo 额外的尺寸：使用`small`、`default`、`large`属性，通过设置`size`属性来配置它们。

```html
<avatar :src="imgUrl" size="small" alt="小头像" />
<avatar :src="imgUrl" size="default" />
<avatar :src="imgUrl" size="large" />
<avatar :src="imgUrl" :size="100" />
```
:::

### 不同形状

:::demo 额外的尺寸：使用`small`、`default`、`large`属性，通过设置`size`属性来配置它们。
```html
<avatar :src="imgUrl" size="default" />
<avatar :src="imgUrl" size="default" shape="square" />
<avatar size="default" />
<avatar size="default" shape="square" />
```
:::
### hover时旋转
:::demo 设置`isRotated`属性可使头像旋转。

 <avatar :src="imgUrl" :size="100" isRotated="isRotated" />
:::

### Attributes

| 属性  | 说明                                           | 类型   | 默认值 |
| ----- | ---------------------------------------------- | ------ | ------ | ------- |
| shape | 指定头像的形状，可选值为 circle、square        | String | circle |
| size  | 设置头像的大小，可选值为 large、small、default | String | Number | default |
| src   | 图片类头像的资源地址                           | String | -      |
| isRotated | 设定头像在hover时是否可旋转                | String  | -      |
| icon | 设置头像的图标类型，参考 Icon 组件 | String | - |
| custom-icon | 自定义图标 | String | - |

# Avatar Events

| 事件名    | 说明                              | 返回值 |
| --------- | --------------------------------- | ------ |
| on-error(未开发)  | 在设置 src 且图片加载不成功时触发 | event  |


### 后续开发功能

额外加入：头像外围镶边，可以是动态效果；可以是方形带圆角的形状
点击头像两种情况：跳转上传头像or网页，发起两种不同的请求