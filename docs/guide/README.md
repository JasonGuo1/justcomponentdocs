

# 安装

尚未在npm发布，以下方法暂无效

## npm安装


推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。


npm i justcomps -S


## CDN
目前可以通过 unpkg.com/justcomps 获取到最新版本的资源，在页面上引入 js 和 css 文件即可开始使用。

<!-- 
<!-- 引入样式 -->
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<!-- 引入组件库 -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script> -->

