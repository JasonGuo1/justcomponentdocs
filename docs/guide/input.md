

## input 输入框

字符输入组件为用户提供了输入字符类信息的能力。

### 类型

- 文本输入
- 文本域
- 数字输入
- 密码输入

### 基础用法
基础的输入框用法。
输入框可以通过输入字符进行交互。有基础的输入框、带有搜索icon的输入框、被禁用的输入框、密码输入框、可点击清空的输入框、输入文本域、以及placeholder功能（可描述输入字段预期值的简短的提示信息）。

<iframe width="100%"  height="350px" src="http://39.101.72.88?type=input"></iframe>


```html
<div>
    <JustInput v-model="value"></JustInput>
    <JustInput v-model="value" icon="search"></JustInput>
    <JustInput disabled></JustInput>
    <JustInput showPassword></JustInput>
    <JustInput clearable></JustInput>
    <JustInput v-model="value1" type="textarea"></JustInput>
    <JustInput readonly placeholder="输入"></JustInput>
    <JustInput placeholder="输入"></JustInput>
    <JustInput placeholder="输入" type="textarea"></JustInput>
</div>
```

### 