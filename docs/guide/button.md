
## Button 按钮

常用的操作按钮。

### 基础用法

基础的按钮用法。


<iframe width="100%"
    height="50px"
 src="http://39.101.72.88?type=btnAble"></iframe>

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div> 
  <JustButton type="primary">主要按钮</JustButton>
  <JustButton type="secondary">次级按钮</JustButton>
  <JustButton type="revert">回退按钮</JustButton>
  <JustButton type="icon"></JustButton>
  <JustButton type="icon" icon="clear"></JustButton>
  <JustButton type="icon" icon="download"></JustButton>
</div>
```
:::

### 禁用状态

按钮不可用状态。
<iframe width="100%" height="50px" src="http://39.101.72.88?type=btnDisable"></iframe>

:::demo 你可以使用`disabled`属性来定义按钮是否可用，它接受一个`Boolean`值。

```html

<div class="btn-disable-wrapper" > 禁用
        <div>
          <JustButton disabled>默认按钮</JustButton>
          <JustButton type="primary" disabled>主要按钮</JustButton>
          <JustButton type="secondary" disabled>次级按钮</JustButton>
          <JustButton type="revert" disabled>回退按钮</JustButton>
        </div>
        <hr>
      </div>
```

:::



### 图标按钮

带图标的按钮可增强辨识度（有文字）或节省空间（无文字）。
<iframe width="100%" height="50px" src="http://39.101.72.88?type=btnIcon"></iframe>


:::demo 设置`icon`属性即可，icon 的列表可以参考 Element 的 icon 组件，也可以设置在文字右边的 icon ，只要使用`i`标签即可，可以使用自定义图标。

```html
 <div>图标按钮
        <JustButton type="icon"></JustButton>
        <JustButton type="icon" icon="clear"></JustButton>
        <JustButton type="icon" icon="download"></JustButton>
      </div>
      
```

:::

### 加载中

点击按钮后进行数据加载操作，在按钮上显示加载状态。
<iframe width="100%" height="50px" src="http://39.101.72.88?type=btnLoading"></iframe>
:::demo 要设置为 loading 状态，只要设置`loading`属性为`true`即可。

```html
   <div class="btn-loading-wrapper" > loading
          <div style="display: flex">
            <!--防止点击后更换位置，需要在父级块状元素处增加style="display: flex;"-->
            <JustButtonLoading type="primary"
              >点击切换loading的主要按钮</JustButtonLoading
            >
            <JustButtonLoading type="secondary"
              >点击切换loading的次级按钮</JustButtonLoading
            >
            <JustButtonLoading type="revert"
              >点击切换loading的回退按钮</JustButtonLoading
            >
            <!-- component event should use emit, default event like 'click' may block -->
          </div>
        </div>
```

:::

### 不同尺寸

Button 组件提供除了默认值以外的三种尺寸，可以在不同场景下选择合适的按钮尺寸。
<iframe width="100%" height="150px" src="http://39.101.72.88?type=btnSize"></iframe>

:::demo 额外的尺寸：`L`、`M`、`S`，通过设置`size`属性来配置它们。

```html
<div class="btn-size-wrapper" >按钮大小
          <div>
            <JustButton type="primary" size="S">主要按钮</JustButton>
            <JustButton type="primary" size="M">主要按钮</JustButton>
            <JustButton type="primary" size="L">主要按钮</JustButton>
          </div>

          <div>
            <JustButton type="secondary" size="S">次级按钮</JustButton>
            <JustButton type="secondary" size="M">次级按钮</JustButton>
            <JustButton type="secondary" size="L">次级按钮</JustButton>
          </div>

          <div>
            <JustButton type="revert" size="S">回退按钮</JustButton>
            <JustButton type="revert" size="M">回退按钮</JustButton>
            <JustButton type="revert" size="L">回退按钮</JustButton>
          </div>
          <hr>
        </div>
```
:::

### 不同形状
<iframe width="100%" height="50px" src="http://39.101.72.88?type=btnShape"></iframe>

:::demo 不同形状：通过设置`round`属性来配置它们。
```html
        <div class="btn-shape-wrapper" >按钮形状 
          <div> 
            <JustButton round="square">方角</JustButton>
            <JustButton round="round">圆角</JustButton>
            <JustButton round="other1">平行四边形</JustButton>
            <JustButton round="other2">椭圆</JustButton>
            <JustButton round="other3">平行四边形</JustButton>
            <JustButton round="other4">活力标题</JustButton>
          </div>
          <hr>
        </div>
```

:::

### Attributes

| 参数        | 说明           | 类型    | 可选值                                                 | 默认值 |
| ----------- | -------------- | ------- | ------------------------------------------------------ | ------ |
| size        | 尺寸           | string  | L / M / S                                  | —      |
| type        | 类型           | string  | primary / secondary / warning / danger / revert / text | —      |
| round       | 形状   | string | round / square / other1 / other2 / other3 / other 4                                                       | square |
| loading     | 是否加载中状态 | string | isloading                                                     | - |
| disabled    | 是否禁用状态   | boolean | —                                                      | false  |

