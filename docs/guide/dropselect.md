

## dropselect 下拉选择

<!-- - 功能要求
    - 支持远程搜索
    - 支持多选
    - 支持多级联动选择
    - 支持选项分组
- 加分项
    - 搜索过程注意做好防抖节流优化
    - 使用composition事件优化输入体验
    - 使用虚拟滚动优化大数据渲染效率 -->

### 基础用法

包含类型：单选，多选，远程搜索（多选），单选分组，单选描述，级联选择，联动选择

<iframe width="100%" height="500px" src="http://39.101.72.88?type=dropselect"></iframe>

:::demo 使用`disabled`定义 dropselect 是否禁用。

```html
 <div class="dropselect-wrapper">
       
      <span>
        单选
        <JustDropSelect type="single" />
        <JustDropSelect type="single" disabled />
      </span>
      <span>
        多选
        <JustDropSelect type="multiple" />
        <JustDropSelect type="multiple" disabled />
      </span> 
      <span>
        多选+搜索
        <JustDropSelect type="multiple-search" />
        <JustDropSelect type="multiple-search" disabled/>
      </span>
      <span>
        单选+分组
        <JustDropSelect type="single-group" />
        单选+带描述
        <JustDropSelect type="single-description" />
      </span>
      <span>
        级联选择
        <JustDropSelect type="single-cascader" />
      </span>
      <span>
          联动选择
        <JustDropSelect type="single-linkage" />
      </span>
      
    </div>
```
:::

### 支持选项分组

<iframe width="100%" height="250px"src="http://39.101.72.88?type=dropselect-single-group"></iframe>

:::demo 支持选项分组

```html
    <div class="dropselect-wrapper" v-if="type == 'dropselect'">
      
      <DropSelect type='single-group' />
     
    </div>
```
:::

### 支持多选

<iframe width="100%" src="http://39.101.72.88?type=dropselect-multiple"></iframe>

:::demo 支持多选

```html
    <div class="dropselect-wrapper" v-if="type == 'dropselect-multiple'">
      <DropSelect type='multiple' />
      <DropSelect type='multiple' disabled />
      <DropSelect type='multiple-search' />
    </div>
```
:::

### 支持远程搜索

<iframe width="100%"  height="300px" src="http://39.101.72.88?type=dropselect-mutiple-search"></iframe>

:::demo 支持远程搜索

```html
    <div  v-if="type == 'dropselect-mutiple-search'">
        <DropSelect type='multiple-search' /> </div>
```
:::

### 支持多级联动选择

<iframe width="100%"  src="http://39.101.72.88?type=dropselect-single-cascader"></iframe>

:::demo 支持多级联动选择

```html
<div  v-if="type == 'dropselect-single-cascader'">
          <singleCascader /></div> 
```
:::


### Attributes

| 参数        | 说明           | 类型    | 可选值                                                 | 默认值 |
| ----------- | -------------- | ------- | ------------------------------------------------------ | ------ |
| size        | 尺寸           | string  | L / M / S                                  | —      |
| type        | 类型           | string  | multiple / single / multiple-search / single-group /single-description/ single-cascader | —      |
| disabled    | 是否禁用状态   | boolean | —                                                      | false  |
