

## datepicker 日期时间选择
<!-- 
- 主体功能
    - 选择特定日期
    - 选择特定日期+时间
    - 选择日期范围
    - 选择时间范围
- 功能要求
    - 支持日期、时间、日期时间-选择https://ant.design/components/date-picker-cn#DatePicker
    - 支持日期、时间、日期时间-范围选择https://ant.design/components/date-picker-cn#RangePicker
    - 支持设定最大/最小不可选日期/时间
    - 支持设定日期展示格式（`format`参数）![[Pasted image 20230116234317.png]]
- 加分项
    - 支持国际化（中英）
    - 支持用户手动输入日期&时间，组件做好数值校验
    - 日期选择中，支持年视图，月视图，日视图
    - 支持特殊日期样式，e.g节假日视图
    - 补充单元测试，覆盖率要求超过60%
 -->

<iframe width="100%"  height="250px"src="http://39.101.72.88?type=dateTimePicker"></iframe>

### 日期与时间设置
用户可以设置日期或时间
:::type=0为日期，type=1为时间

```html
<div>
    <JustDatetimePicker type=0></JustDatetimePicker>
    <JustDatetimePicker type=1></JustDatetimePicker>
</div>
```