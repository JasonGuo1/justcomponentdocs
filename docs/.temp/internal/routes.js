/**
 * Generated by "@vuepress/internal-routes"
 */

import { injectComponentOption, ensureAsyncComponentsLoaded } from '@app/util'
import rootMixins from '@internal/root-mixins'
import GlobalLayout from "C:\\workspace\\justcomponentdocs\\docs\\node_modules\\@vuepress\\core\\lib\\client\\components\\GlobalLayout.vue"

injectComponentOption(GlobalLayout, 'mixins', rootMixins)
export const routes = [
  {
    name: "v-6f80dea4",
    path: "/config/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-6f80dea4").then(next)
    },
  },
  {
    path: "/config/index.html",
    redirect: "/config/"
  },
  {
    name: "v-715f035e",
    path: "/guide/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-715f035e").then(next)
    },
  },
  {
    path: "/guide/index.html",
    redirect: "/guide/"
  },
  {
    name: "v-0a87b3a9",
    path: "/config/using-vue.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-0a87b3a9").then(next)
    },
  },
  {
    name: "v-9a977e26",
    path: "/guide/cascader.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-9a977e26").then(next)
    },
  },
  {
    name: "v-98dcc076",
    path: "/guide/button.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-98dcc076").then(next)
    },
  },
  {
    name: "v-301c213a",
    path: "/guide/checkbox.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-301c213a").then(next)
    },
  },
  {
    name: "v-2bbef953",
    path: "/guide/avator.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-2bbef953").then(next)
    },
  },
  {
    name: "v-0b058ab1",
    path: "/guide/datepicker.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-0b058ab1").then(next)
    },
  },
  {
    name: "v-0956ff83",
    path: "/guide/input.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-0956ff83").then(next)
    },
  },
  {
    name: "v-15127513",
    path: "/guide/dropselect.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-15127513").then(next)
    },
  },
  {
    name: "v-8cde462e",
    path: "/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-8cde462e").then(next)
    },
  },
  {
    path: "/index.html",
    redirect: "/"
  },
  {
    name: "v-bd6c169e",
    path: "/guide/select.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-bd6c169e").then(next)
    },
  },
  {
    path: '*',
    component: GlobalLayout
  }
]