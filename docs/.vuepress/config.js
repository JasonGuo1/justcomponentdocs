module.exports = {
  theme: "",
  title: "VuePress + justcompnent",
  description: "VuePress搭建justcompnent的组件库文档教程示例代码",
  base: "/",
  port: "8081",
  themeConfig: {
    nav: [
      // 一级导航
      // { text: "安装", link: "/guide/" },
      // { text: "组件", link: "/.vuepress/components/Button/" },
      { text: "组件", link: "/guide/" },
      // { text: "demo", link: "/config/" },
      // 下拉列表导航
      {
        text: "项目地址",
        items: [
          {
            text: "comps-gitlab",
            link: "https://gitlab.com/JasonGuo1/jenkins-gitlab-demo",
            target: "_blank",
          },
          {
            text: "docs-gitlab",
            link: "https://gitlab.com/JasonGuo1/justcomponentdocs",
            target: "_blank",
          },
          {
            text: "preview",
            link: "http://39.101.72.88/",
            target: "_blank",
          },
        ],
      },
    ],
    // 禁用导航，与上面的配置是互斥行为。
    // navbar: false
    sidebar: {
      // 配置侧边栏部分
      "/guide/": [
        "/guide/",
      {
        title : "通用型组件",
        children:[ "/guide/button.md" ]
      },
      {
        title : "数据展示型组件",
        children:[ "/guide/avator.md",]
      },
      {
        title : "数据录入型组件",
        children:["/guide/input.md",
        // "/guide/checkbox.md",
        //  "/guide/select.md",
        "/guide/cascader.md",
        "/guide/dropselect.md",
        "/guide/datepicker.md",
        ]
      }

      ],
    },
      
    

    socialLinks: [{ icon: "github", link: "https://github.com/ValeriaWong" }],
  },
  head: [],
  plugins: ["demo-container"],
  markdown: {},
};
