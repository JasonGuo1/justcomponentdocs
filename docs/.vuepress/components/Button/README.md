## Button 按钮

常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
  <ButtonPrimary type="primary" disabled>主要按钮</ButtonPrimary>
  <ButtonPrimary type="secondary" disabled>次级按钮</ButtonPrimary>
  <ButtonPrimary type="revert" disabled>回退按钮</ButtonPrimary>
</div>

<ButtonPrimary type="warning">警告按钮</ButtonPrimary>
<ButtonPrimary type="danger">危险按钮</ButtonPrimary>
```

:::

### 禁用状态

按钮不可用状态。

:::demo 你可以使用`disabled`属性来定义按钮是否可用，它接受一个`Boolean`值。

```html
<div>
  <ButtonPrimary disabled>默认按钮</ButtonPrimary>
  <ButtonPrimary type="primary" disabled>主要按钮</ButtonPrimary>
  <ButtonPrimary type="secondary" disabled>次级按钮</ButtonPrimary>
  <ButtonPrimary type="revert" disabled>回退按钮</ButtonPrimary>
  <ButtonPrimary type="warning" disabled>警告按钮</ButtonPrimary>
  <ButtonPrimary type="danger" disabled>危险按钮</ButtonPrimary>
</div>

<div>
  <ButtonPrimary plain disabled>朴素按钮</ButtonPrimary>
  <ButtonPrimary type="primary" plain disabled>主要按钮</ButtonPrimary>
  <ButtonPrimary type="secondary" plain disabled>成功按钮</ButtonPrimary>
  <ButtonPrimary type="revert" plain disabled>信息按钮</ButtonPrimary>
  <ButtonPrimary type="warning" plain disabled>警告按钮</ButtonPrimary>
  <ButtonPrimary type="danger" plain disabled>危险按钮</ButtonPrimary>
</div>
```

:::

### 文字按钮

没有边框和背景色的按钮。

:::demo

```html
<ButtonPrimary type="text">文字按钮</ButtonPrimary>
<ButtonPrimary type="text" disabled>文字按钮</ButtonPrimary>
```

:::

### 图标按钮

带图标的按钮可增强辨识度（有文字）或节省空间（无文字）。

:::demo 设置`icon`属性即可，icon 的列表可以参考 Element 的 icon 组件，也可以设置在文字右边的 icon ，只要使用`i`标签即可，可以使用自定义图标。

```html
<ButtonPrimary type="primary" icon="el-icon-edit"></ButtonPrimary>
<ButtonPrimary type="primary" icon="el-icon-share"></ButtonPrimary>
<ButtonPrimary type="primary" icon="el-icon-delete"></ButtonPrimary>
<ButtonPrimary type="primary" icon="el-icon-search">搜索</ButtonPrimary>
<ButtonPrimary type="primary"
  >上传<i class="el-icon-upload el-icon--right"></i
></ButtonPrimary>
```

:::

### 按钮组

以按钮组的方式出现，常用于多项类似操作。

:::demo 使用`<ButtonPrimary-group>`标签来嵌套你的按钮。

```html
<ButtonPrimary-group>
  <ButtonPrimary type="primary" icon="el-icon-arrow-left">上一页</ButtonPrimary>
  <ButtonPrimary type="primary"
    >下一页<i class="el-icon-arrow-right el-icon--right"></i
  ></ButtonPrimary>
</ButtonPrimary-group>
<ButtonPrimary-group>
  <ButtonPrimary type="primary" icon="el-icon-edit"></ButtonPrimary>
  <ButtonPrimary type="primary" icon="el-icon-share"></ButtonPrimary>
  <ButtonPrimary type="primary" icon="el-icon-delete"></ButtonPrimary>
</ButtonPrimary-group>
```

:::

### 加载中

点击按钮后进行数据加载操作，在按钮上显示加载状态。

:::demo 要设置为 loading 状态，只要设置`loading`属性为`true`即可。

```html
<div style="display: flex;">
  <!--防止点击后更换位置，需要在父级块状元素处增加style="display: flex;"-->
  <ButtonLoading type="primary">点击切换loading的主要按钮</ButtonLoading>
  <ButtonLoading type="secondary">点击切换loading的次级按钮</ButtonLoading>
  <ButtonLoading type="revert">点击切换loading的回退按钮</ButtonLoading>
</div>
```

:::

### 不同尺寸

Button 组件提供除了默认值以外的三种尺寸，可以在不同场景下选择合适的按钮尺寸。

:::demo 额外的尺寸：`medium`、`small`、`mini`，通过设置`size`属性来配置它们。

```html
<div>
  <ButtonPrimary type="primary" size="S">主要按钮</ButtonPrimary>
  <ButtonPrimary type="primary" size="M">主要按钮</ButtonPrimary>
  <ButtonPrimary type="primary" size="L">主要按钮</ButtonPrimary>
</div>

<div>
  <ButtonPrimary type="secondary" size="S">次级按钮</ButtonPrimary>
  <ButtonPrimary type="secondary" size="M">次级按钮</ButtonPrimary>
  <ButtonPrimary type="secondary" size="L">次级按钮</ButtonPrimary>
</div>

<div>
  <ButtonPrimary type="revert" size="S">回退按钮</ButtonPrimary>
  <ButtonPrimary type="revert" size="M">回退按钮</ButtonPrimary>
  <ButtonPrimary type="revert" size="L">回退按钮</ButtonPrimary>
</div>
```

### 不同形状

```html
<div>
  <ButtonPrimary round="square">方角</ButtonPrimary>
  <ButtonPrimary round="round">圆角</ButtonPrimary>
  <ButtonPrimary round="other1">平行四边形</ButtonPrimary>
  <ButtonPrimary round="other2">椭圆</ButtonPrimary>
  <ButtonPrimary round="other3">平行四边形</ButtonPrimary>
  <ButtonPrimary round="other4">活力标题</ButtonPrimary>
</div>

<div>
  <ButtonPrimary shape="square">方角</ButtonPrimary>
  <ButtonPrimary shape="round">圆角</ButtonPrimary>
  <ButtonPrimary shape="other1">平行四边形</ButtonPrimary>
  <ButtonPrimary shape="other2">椭圆</ButtonPrimary>
  <ButtonPrimary shape="other3">平行四边形</ButtonPrimary>
  <ButtonPrimary shape="other4">活力标题</ButtonPrimary>
</div>
```

:::

### Attributes

| 参数        | 说明           | 类型    | 可选值                                                 | 默认值 |
| ----------- | -------------- | ------- | ------------------------------------------------------ | ------ |
| size        | 尺寸           | string  | medium / small / mini                                  | —      |
| type        | 类型           | string  | primary / secondary / warning / danger / revert / text | —      |
| plain       | 是否朴素按钮   | boolean | —                                                      | false  |
| round       | 是否圆角按钮   | boolean | —                                                      | false  |
| circle      | 是否圆形按钮   | boolean | —                                                      | false  |
| loading     | 是否加载中状态 | boolean | —                                                      | false  |
| disabled    | 是否禁用状态   | boolean | —                                                      | false  |
| icon        | 图标类名       | string  | —                                                      | —      |
| autofocus   | 是否默认聚焦   | boolean | —                                                      | false  |
| native-type | 原生 type 属性 | string  | button / submit / reset                                | button |
