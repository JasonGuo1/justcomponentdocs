---
home: true
heroImage: /heroImage-icon.png
heroText: justcomponent
tagline: vue3+vite+typescript | 第五届字节青训营大项目

actionText: Quick Start →
actionLink: /guide/
features:
- title: contributers 
  details: JasonGuo1 valeriaWong
- title: config 
  details:  gitlab -> jenkins -> nginx
- title: rule 
  details: github flow 
footer: Made by ValeriaWong and JasonGuo1 with 💙

---

### design rule

<div style="display:flex;justify-content: space-between;padding-bottom:40px">
  <div style="display: flex;flex-direction: column;align-items: center;">
    <img style="width:64px " src="../docs/.vuepress/public/consisitent.png" alt="一致性">
    <p style="margin:5px">时间不允许一致性</p>
    <p style="margin:0px;font-size: 12px;color:#666">Consistency</p>
  </div>

  <div style="display: flex;flex-direction: column;align-items: center;">
    <img style="width:64px" src="../docs/.vuepress/public/effeciency.png" alt="效率">
    <p style="margin:5px">不是很高的效率</p>
    <p style="margin:0px;font-size: 12px;color:#666">Efficiency</p>
  </div>
    <div style="display: flex;flex-direction: column;align-items: center;">
    <img style="width:64px;"  src="../docs/.vuepress/public/feedback.png" alt="反馈">
    <p style="margin:5px">可能有反馈</p>
    <p style="margin:0px;font-size: 12px;color:#666"> Feedback</p>
  </div>
  <div style="display: flex;flex-direction: column;align-items: center;">
    <img style="width:64px" src="../docs/.vuepress/public/ui-controler.png" alt="可控">
    <p style="margin:5px">轻微可控</p>
    <p style="margin:0px;font-size: 12px;color:#666">Controllability</p>
  </div>
</div>

